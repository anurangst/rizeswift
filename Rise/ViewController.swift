//
//  ViewController.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 30/11/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate , URLSessionDelegate{
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSingnUp: UIButton!
    @IBOutlet weak var btnRememberMe: UIButton!
    let textFieldContentsKey = "email"
    
    override func viewDidLoad() {
        super.viewDidLoad()
         print("My first project in Swift!!!!!!!!!")
        if(GlobalConstants.Constant.Iphone_5){
           print("This is iPhone5...............")
        }
        
        
        textFieldEmail.delegate = self;
        textFieldPassword.delegate = self;
        self.view.backgroundColor=GlobalConstants.Constant.kColor_Seperator
    
          /*
         ////  DICTIONARY
         var userInfo: [String: String] = [
         "first_name" : "Andrei",
         "last_name" : "Puni",
         "job_title" : "Mad scientist"
         ]
         
         for (key, value) in userInfo {
         print("\(key): \(value)")
         }
         
         if let twoAsInt = userInfo["first_name"] {
         print(twoAsInt) // 2
         }
         
         // Unwaraping the optional using the forced value operator (!)
         print(userInfo["last_name"]!) // 1
         
        
       
        // Do any additional setup after loading the view, typically from a nib.
        var varA = 42
        print(varA)
        
        
        var someInts = [Int]()
        someInts.append(20)
        someInts.append(30)
        someInts += [40]
        var someVar = someInts[0]
        print( "Value of first element is \(someVar)" )
        print( "Value of second element is \(someInts[1])" )
        print( "Value of third element is \(someInts[2])" )
        
        print("Total Elements:\(someInts.count)")
        if(!someInts.isEmpty){
            for item in someInts {
                print(item)
            }
        }
        
        
        var someInts2 = [Int](repeating: 0, count: 3)
        let someVar2 = someInts2[0]
        
        print(someVar2)
        print( "Value of first element is \(someVar2)" )
        let intsC = someInts + someInts2
        var someInts3:[Int] = [10, 20, 30]
        
        
        let num=ls(array:[40,12,-5,78,98])
        if let bounds = ls(array:[8, -6, 2, 109, 3, 71]) {
            print("min is \(bounds.large) and max is \(bounds.small)")
        }
        print("Largest number is: \(num?.large) and smallest number is: \(num?.small)")
        
        ////////////////// CONSTANT   //////////////////////
        let constB:Float = 3.14159 //Type Annotations
        //////////////////  OPTIONAL ///////////////////////
        let myString1:String! //Automatic Unwrapping
        //// Optional
        var myString:String?
        
        myString = "Hello, Swift!"
        
        if let yourString = myString {
            print("Your string has - \(yourString)")
        }else {
            print("Your string does not have a value")
        }
        
        /////////////////// SWICH //////////////////////
        var index = 10
        
        switch index {
        case 100  :
            print( "Value of index is 100")
            fallthrough
        case 10,15  :
            print( "Value of index is either 10 or 15")
            fallthrough
        case 5  :
            print( "Value of index is 5")
        default :
            print   ( "default case")
        }
        
        */
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        // UserDefaults.standard.set("anurantulip@gmail.com", forKey: "email")
       //UserDefaults.standard.synchronize()
        
        
        // load text from NSUserDefaults
        let defaults = UserDefaults.standard
        
        if let textFieldContents = defaults.string(forKey: textFieldContentsKey) {
            textFieldEmail.text = textFieldContents
        } else {
            // focus on the text field if it's empty
            textFieldEmail.becomeFirstResponder()
            
        }
        
       
        if(defaults.string(forKey: textFieldContentsKey) == nil){
            
        }
        
       
        
        
        //print("Print testStr=\(bb)")
    }
    
    
     func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        if (segue.identifier == "segueTest") {
            //Checking identifier is crucial as there might be multiple
            // segues attached to same view
           // var detailVC = segue!.destinationViewController as sliderview;
          //  detailVC.toPass = textField.text
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // SingletonObject.sharedInstance.showAlert(fromController: self, withTitle: "Info", AndMessage: "Hi Anuran")
       // SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
       // SingletonObject().showActivityIndicator(uiView: self.view)  // not accessible as init() is private
        SingletonObject.sharedInstance.setPaddingForTextField(textfield: textFieldEmail)
        SingletonObject.sharedInstance.setPaddingForTextField(textfield: textFieldPassword)
        serviceCall();
        
        print("Service called......");
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func minMax(array: [Int]) -> (min: Int, max: Int)? {
        if array.isEmpty { return nil }
        var currentMin = array[0]
        var currentMax = array[0]
        for value in array[1..<array.count] {
            if value < currentMin {
                currentMin = value
            }else if value > currentMax {
                currentMax = value
            }
        }
        return (currentMin, currentMax)
    }
    func ls(array: [Int]) -> (large: Int, small: Int)? {
        var lar = array[0]
        var sma = array[0]
        for i in array[1..<array.count] {
            if i < sma {
                sma = i
            }else if i > lar {
                lar = i
            }
        }
        return (lar, sma)
    }
    
    func showAlert(msg:String)
    {
        print(msg)
    }
    
    func inputs(no1: Int, no2: Int) -> Int {
        return no1/no2
    }
    
    
    @IBAction func btnSignInAction(sender:UIButton)
    {
        
        sender.isSelected=true;
         print(textFieldEmail.text ?? "")
        textFieldEmail.resignFirstResponder();
        textFieldPassword.resignFirstResponder();
        if textFieldEmail.text?.isEmpty ?? true {
            SingletonObject.sharedInstance.showAlert(fromController: self, withTitle: "Alert", AndMessage: "Enter Email Id")
        }
        /*
        if (textFieldEmail.text?.isEmpty)!
        {
            SingletonObject.sharedInstance.showAlert(fromController: self, withTitle: "Alert", AndMessage: "Eneter Email Id")
        }

        else if(SingletonObject.sharedInstance.checkEmailFormar(strEmail: textFieldEmail.text!)){
        
        }
  */
        else if textFieldPassword.text?.isEmpty ?? true {
            SingletonObject.sharedInstance.showAlert(fromController: self, withTitle: "Alert", AndMessage: "Enter Password")
        }
        
    }
    @IBAction func btnSignUpAction(sender:UIButton)
    {
       SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
    }
    func showAlert() {
        // Create the alert controller
        let alertController = UIAlertController(title: "Title", message: "Message", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    //MARK :- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
        animateViewMoving(up:true, moveValue: 50)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called")
        animateViewMoving(up:false, moveValue: 50)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    func makeGetCall() {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        // Set up the URL request
        let todoEndpoint: String = "https://jsonplaceholder.typicode.com/todos/1"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    DispatchQueue.main.async {
                        //Update your UI here
                        SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                    }
                    return
                }
                // now we have the todo, let's just print it to prove we can access it
                print("The todo is: " + todo.description)
                
                // the todo object is a dictionary
                // so we just access the title using the "title" key
                // so check for a title and print it if we have one
                guard let todoTitle = todo["title"] as? String else {
                    print("Could not get todo title from JSON")
                    return
                }
                print("The title is: " + todoTitle)
                DispatchQueue.main.async {
                    //Update your UI here
                    SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        
        task.resume()
    }
    
    
    
    func serviceCall(){
    
        let myUrl = URL(string: "http://gstdemo.com/all_apps/inke/?action=live_user&user_id=1");
        
        var request = URLRequest(url:myUrl!)
        
        //request.httpMethod = "POST"// Compose a query string
        
      //  let postString = "action=live_user&user_id=1";
        
       // request.httpBody = postString.data(using: String.Encoding.utf8);
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            
            if let error = error
            {
                print(error.localizedDescription)
            }
            else if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Response >>>>>>>>>>>>>>>>>>>>>>")
                    print(dataString!)
                }
            }
            
            
            /*
            if error != nil
            {
                print("error=\(error)")
                return
            }
           
            // You can print out response object
            print("response = \(response)")
            */
            //Let's convert response sent from a server side script to a NSDictionary object:
            do {
                
                
                guard let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject] else {
                    print("error trying to convert data to JSON")
                    DispatchQueue.main.async {
                        //Update your UI here
                        SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                    }
                    return
                }
                // now we have the todo, let's just print it to prove we can access it
                print("The todo is: " + json.description)
                var dict = json
               // print(dict["message_code"]!)
                if dict["message_code"] as? Int == 1 {
                     print(dict["message_code"]!)
                    
                    let user_dataArray: [AnyObject] = dict["user_data"] as! [AnyObject]
                    if(user_dataArray.count>0){
                        let k=user_dataArray.count;
                        for i in 0..<k {
                           // print(user_dataArray[i])
                            let dict:[String:Any] = user_dataArray[i] as! [String : Any]
                            
                            
                            print(dict["name"]!)
                            
                        }
                    }
                   
                }
                /*
                if let twoAsInt = dict["message_code"] {
                    print("message_code>>>>>: \(twoAsInt)")
                }
                
                guard let firstNameValue = json["message_code"] as? Int
                    else {
                        print("Could not get todo title from JSON")
                        return
                }

                print("message_code: \(firstNameValue)")
                
                */
                
                
                           } catch {
                print(error)
            }
            
            
        }
        task.resume()
    
    }
    
    func serviceCall2() {
        let userNameValue = "Anuran"
        
        if SingletonObject.sharedInstance.isStringEmpty(stringValue:userNameValue) == true
        {
            return
        }
        
        // Send HTTP GET Request
        
        // Define server side script URL
        let scriptUrl = "http://swiftdeveloperblog.com/my-http-get-example-script/"
        // Add one parameter
        let urlWithParams = scriptUrl + "?userName=\(userNameValue)"
        // Create NSURL Ibject
        let myUrl = NSURL(string: urlWithParams);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(url:myUrl! as URL);
        
        // Set request HTTP method to GET. It could be POST as well
        request.httpMethod = "GET"
        
        // If needed you could add Authorization header value
        // Add Basic Authorization
        /*
         let username = "myUserName"
         let password = "myPassword"
         let loginString = NSString(format: "%@:%@", username, password)
         let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
         let base64LoginString = loginData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
         request.setValue(base64LoginString, forHTTPHeaderField: "Authorization")
         */
        
        // Or it could be a single Authorization Token value
        //request.addValue("Token token=884288bae150b9f2f68d8dc3a932071d", forHTTPHeaderField: "Authorization")
        
        // Excute HTTP Request
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            
            // Convert server json response to NSDictionary
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    // Print out dictionary
                    print(convertedJsonIntoDict)
                    
                    // Get value by key
                    let firstNameValue = convertedJsonIntoDict["userName"] as? String
                    print(firstNameValue!)
                    
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }
        
        task.resume()
    }
    func nsurlSessionCall() {
        let url = URL(string:"Download URL")!
        let req = NSMutableURLRequest(url:url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        
        let task : URLSessionDownloadTask = session.downloadTask(with: req as URLRequest)
        task.resume()
        
       
    }
    
    //urlSession  Protocol
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
    }
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        print("downloaded \(100*writ/exp)" as AnyObject)
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL){
        
    }

}


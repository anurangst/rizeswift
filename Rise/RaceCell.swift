//
//  RaceCell.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 13/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit

class RaceCell: UITableViewCell {
    @IBOutlet weak var lblRaceTitle:UILabel!
    @IBOutlet weak var imgRace:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

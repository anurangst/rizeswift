//
//  todaysRace.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 21/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import Alamofire
class todaysRace: BaseviewController , UITableViewDelegate ,UITableViewDataSource {
    @IBOutlet weak var tblview:UITableView!
    var  raceArray = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblview.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        if (segue.identifier == "todaysRace") {
            //Checking identifier is crucial as there might be multiple
            // segues attached to same view
            var todaysRaceVC = segue!.destination as! todaysRace
            todaysRaceVC.raceArray = raceArray
            
        }
    }
 */
    func todaysRace()
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "apitodaysracelist"] as [String : Any]
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = (result as? [String : Any])?["message"] as! String
                    print(message_code)
                    if(message_code == "1"){
                        self.raceArray = ((result as? [String : Any])?["todays_race"] as? [Any])!
                        Helper.setTableview(tblview:self.tblview)
                        print("Loading Data in tableview ............")
                        
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
                //  UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // handle success
                
        }
    }
    
    // MARK : tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Total Count: \(raceArray.count)")
        return raceArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        var cell: RaceCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RaceCell
        if cell == nil {
            tableView.register(UINib(nibName: "RaceCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RaceCell
        }
        let raceDict:[String:Any] = raceArray[indexPath.row] as! [String : Any]
        cell.lblRaceTitle.text = (raceDict["race_title"] as! String)
        cell.imgRace.sd_setImage(with: URL(string:(raceDict["race_image"] as! String)))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
       // let raceDict:[String:Any] = raceArray[indexPath.row] as! [String:Any]
        let raceDict:[String:Any] = ["index" : indexPath.row]
        NotificationCenter.default.post(name:Notification.Name("FrameSelectedNotification"), object: nil, userInfo: raceDict)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnBackAction(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }


}

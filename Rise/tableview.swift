//
//  tableview.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 05/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import Foundation
import UIKit
class listingData: UIViewController ,UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var tableView:UITableView!
    let animals: [String] = ["Horse", "Cow", "Camel", "Sheep", "Goat"]
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "Cell"
    // Register the table view cell class and its reuse id
    override func viewDidLoad() {
        super.viewDidLoad()
         print("Tableview in Swift!!!!!!!!!")
        // Register the table view cell class and its reuse id
        //tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
       // self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        //tableView.reloadData();
        self.tableView.dataSource=self
        self.tableView.delegate=self
        self.tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return animals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        
        
        // Trying to reuse a cell
        /*
        let cellIdentifier = "ElementCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            ?? UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        */
        let identifier = "Cell"
        var cell: customCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? customCell
        if cell == nil {
            tableView.register(UINib(nibName: "customCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? customCell
        }
        
        
        cell.lblTitle.text = animals[indexPath.row]
        cell.selectionStyle = .none
        // Adding the right informations
        //cell.textLabel?.text = animals[indexPath.row]
        //cell.detailTextLabel?.text = element.name
        
        // Returning the cell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

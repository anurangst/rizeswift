//
//  tabController.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 23/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit

class tabController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let first = Helper.storyBoard().instantiateViewController(withIdentifier: "Races") as! Races
        let navRaceContrl : UINavigationController = UINavigationController.init(rootViewController: first )
        first.tabBarItem = UITabBarItem.init(title: "Race", image: UIImage(named: "tabRace")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "tabRaceActive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal))

        let second = Helper.storyBoard().instantiateViewController(withIdentifier: "MyRaces") as! MyRaces
        let navProfileContrl : UINavigationController = UINavigationController.init(rootViewController: second )
        second.tabBarItem = UITabBarItem.init(title: "Profile", image: UIImage(named: "tabProfile")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "tabProfileActive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal))

        let third = Helper.storyBoard().instantiateViewController(withIdentifier: "Camera") as! Camera
        let navCaemraContrl : UINavigationController = UINavigationController.init(rootViewController: third )
        
        
        third.tabBarItem = UITabBarItem.init(title: "Camera", image: UIImage(named: "tabCamera")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "tabCameraActive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal))
    
        let fourth = Helper.storyBoard().instantiateViewController(withIdentifier: "Inbox") as! Inbox
        fourth.tabBarItem = UITabBarItem.init(title: "Inbox", image: UIImage(named: "tabInbox")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "tabInboxActive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal))
         let navInboxContrl : UINavigationController = UINavigationController.init(rootViewController: fourth )
      //  fourth.tabBarItem.isEnabled = false  ///////////
        
        
        
        let fifth = Helper.storyBoard().instantiateViewController(withIdentifier: "Coupons") as! Coupons
        let navCouponContrl : UINavigationController = UINavigationController.init(rootViewController: fifth )
        fifth.tabBarItem = UITabBarItem.init(title: "Coupons", image: UIImage(named: "tabCoupon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "tabCouponActive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal))
     
        self.viewControllers = [navRaceContrl, navProfileContrl , navCaemraContrl , navInboxContrl , navCouponContrl]
        
        self.tabBar.barTintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

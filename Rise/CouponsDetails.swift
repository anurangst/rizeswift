//
//  CouponsDetails.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import Alamofire
class CouponsDetails: BaseviewController {
    @IBOutlet weak var lblMerchantName:UILabel!
    @IBOutlet weak var lblRaceTitle:UILabel!
    @IBOutlet weak var lblCouponTitle:UILabel!
    @IBOutlet weak var lblCouponPackTitle:UILabel!
    @IBOutlet weak var txtCouponDesc:UITextView!
    @IBOutlet weak var txtTermsAndCondt:UITextView!
    @IBOutlet weak var lblExpireDate:UILabel!
    @IBOutlet weak var imgMerchantLogo:UIImageView!
    @IBOutlet weak var imgCouponBanner:UIImageView!
    @IBOutlet weak var btnRedeemCoupon:UIButton!
    var dictCoupon: [String:String] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCouponTitle.text = dictCoupon["coupon_name"]
        lblCouponPackTitle.text = dictCoupon["coupon_group"]
        lblRaceTitle.text = dictCoupon["race_title"]
        lblMerchantName.text = dictCoupon["marchant_name"]
        txtCouponDesc.text = dictCoupon["description"]
        txtTermsAndCondt.text = dictCoupon["term_condition"]
        imgCouponBanner.sd_setImage(with: URL(string:dictCoupon["coupon_logo"]!))
        imgMerchantLogo.sd_setImage(with: URL(string:dictCoupon["marchant_logo"]!))
        
        if (dictCoupon["is_redeem"]=="1")
        {
            self.btnRedeemCoupon.isHidden = true
        }
        if( dictCoupon["exp_date"] != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd";
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "dd-MMM-yyyy";
            lblExpireDate.text  = "Expire date: " + dateFormatter2.string(from: dateFormatter.date(from: dictCoupon["exp_date"]!)!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction()
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnRedeemAction(sender:UIButton)
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "apiredeemcoupon" , "coupon_id": dictCoupon["coupon_name"]! , "user_id":UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String] as [String : Any]
        print (parameters)
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    if(message_code == "1"){
                        self.btnRedeemCoupon.isHidden = true
                        Helper.showAlert(fromController: self, withTitle: "Success!", AndMessage: "You have reddemed this coupon successfully.")
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: "Sorry there are some internal error. Please try later.")
                    }
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
        }
    }

}

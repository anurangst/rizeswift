//
//  UIViewController+hidekeypad.swift
//  socialLogin
//
//  Created by GOLDSYNC TECH on 09/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import Foundation
import  UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}




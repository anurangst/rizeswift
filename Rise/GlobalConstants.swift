//
//  GlobalConstants.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 05/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import Foundation
import UIKit



class GlobalConstants {
    static let BASE_URL = "http://gstdemo.com/races/webservice"
    static let SCREEN_WIDTH : Int = Int(UIScreen.main.bounds.size.width)
    struct Constant {
        static let BASE_URL = "http://gstdemo.com/races/webservice"
        //static let redColor =  UIColor(rgba: "#FFEDB4")
        private static let testInt = 1
       // static let SCREEN_WIDTH : Int = Int(UIScreen.main.bounds.size.width)
        //  Device IPHONE
        static let Iphone_4s : Bool =  (UIScreen.main.bounds.size.height == 480)
        static let Iphone_5 : Bool =  (UIScreen.main.bounds.size.height == 568)
        static let Iphone_6 : Bool =  (UIScreen.main.bounds.size.height == 667)
        static let Iphone_6_Plus : Bool =  (UIScreen.main.bounds.size.height == 736)
        //  COLOR CONSTANT
        
        static let kColor_Seperator:UIColor = UIColor(red: 53.0/255.0, green: 126.0/255.0, blue: 167.0/255.0, alpha: 1.0)
        static let kColor_orange: UIColor = UIColor(red: 255.0/255.0, green: 147.0/255.0, blue: 38.0/255.0, alpha: 1.0)
        static let kColor_NonCompliant: UIColor = UIColor(red: 190.0/255.0, green: 15.0/255.0, blue: 52.0/255.0, alpha: 1.0)
        
    }
}
 
 

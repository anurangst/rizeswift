//
//  SingletonObject.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 01/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import Foundation
import UIKit

class SingletonObject {
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var loadingText: UILabel = UILabel()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    private init() { }
    static let sharedInstance = SingletonObject()
    
    func checkEmailFormar(strEmail: String)->Bool
    {
        return true
    }
    
    func showAlert(fromController controller: UIViewController, withTitle title: String, AndMessage message:String) {
        // Create the alert controller
        let alertController = UIAlertController(title:title, message:message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    /*
     Show customized activity indicator,
     actually add activity indicator to passing view
     
     @param uiView - add activity indicator to this view
     */
    func showActivityIndicator(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80);
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        loadingText.frame = CGRect(x: 0, y:60, width: 80, height: 20);
        loadingText.text="Processing..";
        loadingText.textAlignment=NSTextAlignment.center
        loadingText.font=UIFont(name: loadingText.font.fontName, size: 11)
        loadingText.textColor=UIColor.white;
        
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2);
        
        loadingView.addSubview(loadingText)
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    /*
     Hide activity indicator
     Actually remove activity indicator from its super view
     
     @param uiView - remove activity indicator from this view
     */
    func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    /*
     Define UIColor from hex value
     
     @param rgbValue - hex color value
     @param alpha - transparency level
     */
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func setPaddingForTextField(textfield: UITextField)  {
        let leftPaddingView = UIView(frame:CGRect(x: 0, y: 0, width:10, height:textfield.frame.size.height))
        textfield.leftView = leftPaddingView
        textfield.leftViewMode = UITextFieldViewMode.always
    }
    
    func isStringEmpty(stringValue:String) -> Bool
    {
        var returnValue = false
        
        if stringValue.isEmpty  == true
        {
            returnValue = true
            return returnValue
        }
        
        
        
        // Make sure user did not submit number of empty spaces
        let str   = stringValue.trimmingCharacters(in: NSCharacterSet.whitespaces)
        
        if(str.isEmpty == true)
        {
            returnValue = true
            return returnValue
            
        }
        
        return returnValue
        
    }
    
    
    static func loadImageFromUrl(url: String, view: UIImageView){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async {
                    //Update your UI here
                    view.image = UIImage(data: data)
                }
            }
        }
        
        // Run task
        task.resume()
    }
}

//
//  sliderview.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 06/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit

import FBSDKCoreKit
import FacebookLogin
import Alamofire
class sliderview: UIViewController, UIScrollViewDelegate , GIDSignInDelegate , GIDSignInUIDelegate , LoginButtonDelegate{
    @IBOutlet weak var scrlview:UIScrollView!
    @IBOutlet weak var pageCntrl:UIPageControl!
    @IBOutlet weak var slide1:UIImageView!
    var img:UIImageView!
    var nextPage :Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden=true
       scrlview.delegate=self
       scrlview.isPagingEnabled=true;
        pageCntrl.numberOfPages=3;
        // pageCntrl.autoresizingMask=UIViewAutoresizingNone;
       
        scrlview.contentSize=CGSize(width: GlobalConstants.SCREEN_WIDTH*4, height: Int(scrlview.frame.size.height))
        //scrlview.contentSize=CGSize(width: GlobalConstants.Constant.SCREEN_WIDTH*4, height: scrlview.frame.size.height)
        
        //scrollView.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        //  [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
        
       
        
       var timer = Timer.scheduledTimer(timeInterval:2, target: self, selector: #selector(scrollingTimer), userInfo: nil, repeats: true);
        
       // SingletonObject.loadImageFromUrl(url: "http://www.apple.com/euro/ios/ios8/a/generic/images/og.png", view: slide1)
        
       
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    
    func scrollingTimer() {
        
        
        let contentOffset:CGFloat = scrlview.contentOffset.x;
        // calculate next page to display
         nextPage = (Int)(contentOffset/scrlview.frame.size.width) + 1 ;
        // if page is not 10, display it
        if   (nextPage != 3)  {
            scrlview.scrollRectToVisible(CGRect(x: nextPage * Int(scrlview.frame.size.width), y: 0, width:Int(scrlview.frame.size.width), height:Int(scrlview.frame.size.height)), animated: true)
            pageCntrl.currentPage=nextPage;
            // else start sliding form 1 :)
        }

        if( nextPage == 3)  {
                        scrlview.scrollRectToVisible(CGRect(x: nextPage * Int(scrlview.frame.size.width), y: 0, width:Int(scrlview.frame.size.width), height:Int(scrlview.frame.size.height)), animated: true)
            pageCntrl.currentPage=0;
            // else start sliding form 1 :)
        }
    }
    /*
    func scrollingTimer() {
    
    CGFloat contentOffset = scrlview.contentOffset.x;
    // calculate next page to display
    nextPage = (int)(contentOffset/scrlview.frame.size.width) + 1 ;
    // if page is not 10, display it
     
    if( nextPage==3)  {
    [scrlview scrollRectToVisible:CGRectMake(nextPage*scrlview.frame.size.width, 0, scrlview.frame.size.width, scrlview.frame.size.height) animated:YES];
    pageCntrl.currentPage=0;
    // else start sliding form 1 :)
    }
        }
    -(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
    {
    [self scrollViewDidEndDecelerating:scrollView];
    }
    
    - (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    // The key is repositioning without animation
    
    if (scrlview.contentOffset.x == SCREEN_WIDTH*3) {
    // user is scrolling to the right from image 10 to image 1.
    // reposition offset to show image 1 that is on the left in the scroll view
    [scrlview scrollRectToVisible:CGRectMake(0, 0, scrlview.frame.size.width, scrlview.frame.size.height) animated:NO];
    nextPage=0;
    }
    
    }
*/
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollViewDidEndDecelerating(scrlview)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (scrlview.contentOffset.x == CGFloat(GlobalConstants.SCREEN_WIDTH * 3)) {
            scrlview.scrollRectToVisible(CGRect(x: 0, y: 0, width:Int(scrlview.frame.size.width), height:Int(scrlview.frame.size.height)), animated: false)
            nextPage=0
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    @IBAction func btnFacebookLogin(sender:UIButton)
    {
        NSLog("pressed!")
        UserDefaults.standard.set("facebook", forKey: "loginType")
        UserDefaults.standard.synchronize()
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                print(accessToken.userId!)
                //self.btnFacebookLogin.setTitle("Logout", for: .normal)
                
                self.returnUserData()
                
                
            }
        }
    }
   
    
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        
        switch result {
        case .success(let grantedPermissions, _, let accessToken):
            print("facebook success")
            print(accessToken.authenticationToken)
            break
        case .cancelled:
            print("facebook cancelled")
            break
            
        case .failed(let error):
            print("facebook error")
            break
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("User has logged out")
    }
    
    
    func returnUserData()
    {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]).start(completionHandler: {(connection, result, error) -> Void in
            if error != nil{
                print("Error : \(error.debugDescription)")
            }else{
                
                print("userInfo is \(result!))")
                
                let userData = result as! [String:AnyObject]
                // print(userData["name"]!)
                let type = (result as? [String : String])?["Type"]
                print(userData)
                var firstName: String = ""
                var lastName: String  = ""
                var email: String  = ""
                var gender: String = ""
                
                if (userData["first_name"] == nil){
                    firstName = userData["first_name"] as! String
                }
                if (userData["last_name"] == nil){
                    lastName = userData["last_name"] as! String
                }
                if (userData["email"] == nil){
                    email = userData["email"] as! String
                }
                if (userData["gender"] == nil){
                    gender = userData["gender"] as! String
                }
                
                let userDict = ["action": "apisocial", "first_name":firstName, "last_name": lastName , "email":email, "gender": gender] as [String : Any]
                
                
                self.socialLogin(parameters: userDict)
                // let type = (self.data[indexPath.row] as? [String : String])?["Type"]
                
                
                
            }
        })
    }
    
    func logout(){
        /*
        let loginManager = LoginManager()
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)
        loginManager.logOut()
        print("loggedout")
 */
    }
    
    
    
    
    
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    ///  GIDSignInDelegate  protocols
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let firstName=user.profile.name;
            let lastName=user.profile.name;
            print(fullName!)
            print(email!)
            
            let userDict = ["action": "apisocial", "first_name":firstName!, "last_name": lastName! , "email":email!, "gender": ""] as [String : Any]
            self.socialLogin(parameters: userDict)
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    @IBAction func btnGoogleLogin(sender:UIButton)
    {
        UserDefaults.standard.set("google", forKey: "loginType")
        UserDefaults.standard.synchronize()
        GIDSignIn.sharedInstance().signIn()
    }
   
    
    @IBAction func didTapSignOut(sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    
    
    func socialLogin(parameters: [String : Any])
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        print("Social Login function called.........")
       //let parameters = ["action": "apisocial", "first_name":txtFirstName.text!, "last_name": txtLastName.text! , "email":txtEmail.text!, "gender": "Male"] as [String : Any]
      //  let parameters = dict as? [String : Any]
        print(GlobalConstants.BASE_URL)
        print(parameters)
        
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                //debugPrint(response)
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                //  https://grokswift.com/rest-with-alamofire-swiftyjson/
                switch response.result {
                case .success(let JSON):
                    
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = (result as? [String : Any])?["message"] as! String
                    print(message_code)
                    if(message_code == "1"){
                        UserDefaults.standard.set((result as? [String : Any])?["user_data"], forKey: "user_data")
                        UserDefaults.standard.synchronize()
                        Helper.loadTabBar()
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                    
                    /*
                     let JSON = result as! NSDictionary
                     let message_code = (result as? [String : Any])?["message_code"]
                     let user_data = ((result as? [String : Any])?["user_data"] as? [String : Any])?["name"]
                     
                     let uData = JSON["user_data"] as! NSDictionary
                     print(uData["name"]!)
                     */
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                // handle success
                
        }
        
    }
    
}

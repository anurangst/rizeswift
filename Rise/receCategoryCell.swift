//
//  receCategoryCell.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 27/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit

class receCategoryCell: UITableViewCell {
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblDistance:UILabel!
    @IBOutlet weak var lblTitle:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

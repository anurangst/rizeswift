//
//  AppDelegate.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 30/11/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AlamofireNetworkActivityIndicator
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
           //NetworkActivityIndicatorManager.shared.isEnabled = true
          // NetworkActivityIndicatorManager.shared.startDelay = 1.0
         //  NetworkActivityIndicatorManager.shared.completionDelay = 0.2
        // Override point for customization after application launch.
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if (UserDefaults.standard.dictionary(forKey: "user_data") != nil ){
            self.window?.rootViewController = tabController()
            let selectedColor   = UIColor(red: 114.0/255.0, green: 211.0/255.0, blue: 237.0/255.0, alpha: 1.0)
            let normalColor   = UIColor(red: 189.0/255.0, green: 193.0/255.0, blue: 201.0/255.0, alpha: 1.0)
           UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:selectedColor], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:normalColor], for: .normal)
        }else{
            let navContrl : UINavigationController = UINavigationController.init(rootViewController: Helper.storyBoard().instantiateViewController(withIdentifier: "sliderview") as! sliderview)
            self.window?.rootViewController = navContrl
        }
        
        self.window?.makeKeyAndVisible()
        return true
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print ("###### Returned to App ######");
        
        if(UserDefaults.standard.string(forKey: "loginType")=="facebook"){
            let handled = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            return handled
        }else{
            return GIDSignIn.sharedInstance().handle(url,sourceApplication:sourceApplication,annotation: annotation)
        }
        
        
        
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //App activation code
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
       // FBSDKAppEvents.activateApp()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


//
//  collectionViewController.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 06/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit

class collectionViewController:  UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var collectionview:UICollectionView!
    let animals: [String] = ["Horse", "Cow", "Camel", "Sheep", "Goat"]
    // cell reuse id (cells that scroll out of view can be reused)
    let reuseIdentifier = "customCollectionViewCell"
    // Register the table view cell class and its reuse id
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Collectionview in Swift!!!!!!!!!")
        // Register the table view cell class and its reuse id
        //tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        // self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        //tableView.reloadData();
        // This works, by nib
        
        let nib=UINib(nibName: "customCollectionViewCell", bundle: nil)
        collectionview.register(nib,forCellWithReuseIdentifier:reuseIdentifier)
        
        self.collectionview.dataSource=self
        self.collectionview.delegate=self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return animals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // Set cell width to 100%
       
        return CGSize(width: GlobalConstants.SCREEN_WIDTH/2, height: GlobalConstants.SCREEN_WIDTH/2)
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        // get a reference to our storyboard cell
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! customCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
         cell.lblTitle.text=animals[indexPath.row];
        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    

}

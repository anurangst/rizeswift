//
//  MyRaces.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import Alamofire
class MyRaces: BaseviewController , UITableViewDelegate ,UITableViewDataSource , UIPickerViewDelegate, UIPickerViewDataSource ,UICollectionViewDataSource,UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    @IBOutlet weak var tblview:UITableView!
    @IBOutlet weak var scrlMyRace:UIScrollView!
    @IBOutlet weak var scrlEditProfile:UIScrollView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblDistanceRun:UILabel!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtContactNo:UITextField!
    @IBOutlet weak var txtGender:UITextField!
    @IBOutlet weak var txtNationality:UITextField!
    @IBOutlet weak var txtCountryOfResidence:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtState:UITextField!
    @IBOutlet weak var txtZipCode:UITextField!
    @IBOutlet weak var txtDob:UITextField!
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var btnEditProfile:UIButton!
    @IBOutlet weak var btnMyRace:UIButton!
    @IBOutlet weak var btnMyPhotos:UIButton!
    @IBOutlet weak var viewSelectedBAr:UIView!
    var selectedDob :String = "1900-01-01"
    var picker: UIPickerView!
    var gengerArray = ["Male","Female"]
    var upcomingRaceArray =  [Any]()
    var completedRaceArray =  [Any]()
    var myPhotosArray = [Any]()
    var capturedImage: UIImage!
    
    @IBOutlet weak var collectionview:UICollectionView!
    
    // cell reuse id (cells that scroll out of view can be reused)
    let reuseIdentifier = "customCollectionViewCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblDistanceRun.text = ""
        self.tblview.isHidden=true
        self.tblview.sectionHeaderHeight = 0
        self.tblview.sectionFooterHeight = 0
        self.viewSelectedBAr.center.x = self.btnEditProfile.center.x
       
        self.lblUserName.text = (UserDefaults.standard.dictionary(forKey: "user_data")?["first_name"] as! String) + " " + (UserDefaults.standard.dictionary(forKey: "user_data")?["last_name"] as! String)
        self.hideKeyboardWhenTappedAround()
        
        
        let nib=UINib(nibName: "customCollectionViewCell", bundle: nil)
        collectionview.register(nib,forCellWithReuseIdentifier:reuseIdentifier)
        
        self.collectionview.dataSource=self
        self.collectionview.delegate=self
        
        let nibHeader = UINib(nibName: "TableSectionHeader", bundle: nil)
        self.tblview.register(nibHeader,forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        setUserdata();
    }

    
    override func viewWillAppear(_ animated: Bool) {
        //self.tblview = UITableView(frame: self.tblview.frame, style: .grouped)

        if(btnMyRace.isSelected){
            self.myRace()
        }
        if(btnMyPhotos.isSelected){
            self.myPhotos()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUserdata()
    {
        self.view.backgroundColor = UIColor.gray
        scrlEditProfile.isHidden = false
        scrlMyRace.isHidden = true
        collectionview.isHidden = true
        
        let userDict: [String:String] = UserDefaults.standard.dictionary(forKey: "user_data") as! [String : String]
        
        print ("User Data >>>\(userDict)")
        txtEmail.text = userDict["email"]
        txtFirstName.text = userDict["first_name"]
        txtLastName.text = userDict["last_name"]
        txtGender.text = userDict["gender"]
        
        self.imgUser.sd_setImage(with: URL(string:userDict["profile_image"]!), placeholderImage: UIImage.init(named: "profile"))
        if( userDict["dob"] != "1900-01-01"){
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy-MM-dd";

          let dateFormatter2 = DateFormatter()
          dateFormatter2.dateFormat = "dd-MM-yyyy";
          txtDob.text  = dateFormatter2.string(from: dateFormatter.date(from: userDict["dob"]!)!)
        }
        
        txtAddress.text = userDict["address"]
        txtCity.text = userDict["city"]
        txtState.text = userDict["state"]
        txtZipCode.text = userDict["zipcode"]
        txtNationality.text = userDict["nationality"]
        txtCountryOfResidence.text = userDict["country"]
        scrlEditProfile.contentSize=CGSize.init(width:GlobalConstants.SCREEN_WIDTH, height: 1400)
        Helper.setPaddingForTextField(textfield: txtEmail)
        Helper.setPaddingForTextField(textfield: txtFirstName)
        Helper.setPaddingForTextField(textfield: txtLastName)
        Helper.setPaddingForTextField(textfield: txtDob)
        Helper.setPaddingForTextField(textfield: txtGender)
        Helper.setPaddingForTextField(textfield: txtContactNo)
        Helper.setPaddingForTextField(textfield: txtCountryOfResidence)
        Helper.setPaddingForTextField(textfield: txtAddress)
        Helper.setPaddingForTextField(textfield: txtCity)
        Helper.setPaddingForTextField(textfield: txtState)
        Helper.setPaddingForTextField(textfield: txtZipCode)
        Helper.setPaddingForTextField(textfield: txtNationality)
         self.defineDatePicker()
        picker = UIPickerView ()
        picker.dataSource = self
        picker.delegate = self
        txtGender.inputView = picker
    }
    func defineDatePicker()
    {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        txtDob.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        //dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd-MM-yyyy";
        //dateFormatter.timeStyle = DateFormatter.Style.none
        txtDob.text = dateFormatter.string(from: sender.date)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd";
        selectedDob = dateFormatter2.string(from: sender.date)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
       return 2
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gengerArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtGender.text = gengerArray[row]
    }
    @IBAction func btnBackAction(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction  func btnLogoutAction(sender:UIButton)
    {
        
        let alertController = UIAlertController(title:title, message:"Are you sure to logout?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            UserDefaults.standard.removeObject(forKey: "user_data")
            UserDefaults.standard.synchronize()
           let sliderviewOBJ  = Helper.storyBoard().instantiateViewController(withIdentifier: "sliderview") as! sliderview
            Helper.appDelegate().window??.rootViewController = sliderviewOBJ
        }
        
         let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
         UIAlertAction in

         }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    @IBAction  func btnEditProfileAction(sender:UIButton)
    {
        self.view.backgroundColor = UIColor.gray
         self.scrlEditProfile.isHidden=false
        btnMyRace.isSelected = false
        btnMyPhotos.isSelected = false
        sender.isSelected = true
        setUserdata()
        UIView.animate(withDuration: 0.2, animations: {
            self.viewSelectedBAr.center.x = self.btnEditProfile.center.x
        }, completion: {
            (value: Bool) in
            
        })
        
        
    }
    @IBAction  func btnMyRaceAction(sender:UIButton)
    {
        self.view.backgroundColor = UIColor.white
        sender.isSelected = true
        btnEditProfile.isSelected = false
        btnMyPhotos.isSelected = false
        myRace()
        UIView.animate(withDuration: 0.2, animations: {
            self.viewSelectedBAr.center.x = self.btnMyRace.center.x
        }, completion: {
            (value: Bool) in
            
        })
    }
    @IBAction  func btnMyPhotoAction(sender:UIButton)
    {
        
        sender.isSelected = true
        btnEditProfile.isSelected = false
        btnMyRace.isSelected = false
        myPhotos()
        UIView.animate(withDuration: 0.2, animations: {
            self.viewSelectedBAr.center.x = self.btnMyPhotos.center.x
        }, completion: {
            (value: Bool) in
            
        })
    }
    @IBAction func btnSubmitAction(sender:UIButton)
    {
    
        if (Helper.isStringEmpty(stringValue: txtFirstName.text!))
        {
            Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: "Please Enter First Name.")
        }
            
        else if (Helper.isStringEmpty(stringValue: txtLastName.text!))
        {
            Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: "Please Enter Last Name.")
        }
        else
        {
           // self.editProfile()
            self.editProfilePicture()
        }
        
    }
    @IBAction  func btnChangeProfilePictureAction(sender:UIButton)
    {
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let saveAction = UIAlertAction(title: "Choose from Album", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
        
    }

    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        print("Delegate Called..........")
        //imgUser.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgUser.image = info[UIImagePickerControllerEditedImage] as? UIImage
        picker .dismiss(animated: true, completion: nil)
        /*
         
         var imageData = UIImageJPEGRepresentation(imagePicked.image, 0.6)
         var compressedJPGImage = UIImage(data: imageData)
         UIImageWriteToSavedPhotosAlbum(compressedJPGImage, nil, nil, nil)
         
         var alert = UIAlertView(title: "Wow",
         message: "Your image has been saved to Photo Library!",
         delegate: nil,
         cancelButtonTitle: "Ok")
         alert.show()
         
         */
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("picker cancel.")
    }
   
    func editProfilePicture(){
        
        
        let parameters = ["action": "apieditprofile", "user_id":UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String, "email":txtEmail.text!, "first_name": txtFirstName.text!, "last_name": txtLastName.text!, "gender": txtGender.text!, "dob": selectedDob, "contact_no": txtContactNo.text!, "zipcode": txtZipCode.text!, "city": txtCity.text!, "state": txtState.text!, "nationality": txtNationality.text!, "country": txtCountryOfResidence.text! , "address": txtAddress.text!] as [String : Any]
       let user_id = UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String
        let generatedUrl = GlobalConstants.BASE_URL
        
        // let generatedUrl = GlobalConstants.BASE_URL + "?action=profileimgupload&race_id=1&user_id=" + user_id
        print (generatedUrl)
        
        let URL = try! URLRequest(url: generatedUrl, method: .post, headers: nil)
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            multipartFormData.append(UIImagePNGRepresentation(Helper.resizeImage(image: self.imgUser.image!, newWidth: 200)
                )!, withName: "fileToUpload", fileName: "picture.png", mimeType: "image/png")
        }, with: URL, encodingCompletion: {
            encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print("SUCCESS RESPONSE: \(response.result.value!)")
                    SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                    if let result = response.result.value {
                        print("Data............")
                        let message_code = (result as? [String : Any])?["message_code"] as! String
                        let message = (result as? [String : Any])?["message"] as! String
                        print(message_code)
                        if(message_code == "1"){
                            UserDefaults.standard.set((result as? [String : Any])?["user_data"], forKey: "user_data")
                            UserDefaults.standard.synchronize()
                            Helper.showAlert(fromController: self, withTitle: "Info", AndMessage: "Profile data updated successfully")
                            
                            
                        }else{
                            Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                        }
                        
                        
                    }
                    
                }
            case .failure(let encodingError):
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                print("ERROR RESPONSE: \(encodingError)")
            }
        })
        
        
        /*
         for (key, value) in parameters {
         multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
         }
         */


    }
    func editProfile()
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        print("Login function called.........")
        let parameters = ["action": "apieditprofile", "user_id":UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String, "email":txtEmail.text!, "first_name": txtFirstName.text!, "last_name": txtLastName.text!, "gender": txtGender.text!, "dob": selectedDob, "contact_no": txtContactNo.text!, "zipcode": txtZipCode.text!, "city": txtCity.text!, "state": txtState.text!, "nationality": txtNationality.text!, "country": txtCountryOfResidence.text! , "address": txtAddress.text!] as [String : Any]
        print (parameters)
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                //debugPrint(response)
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                //  https://grokswift.com/rest-with-alamofire-swiftyjson/
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = (result as? [String : Any])?["message"] as! String
                    print(message_code)
                    if(message_code == "1"){
                        UserDefaults.standard.set((result as? [String : Any])?["user_data"], forKey: "user_data")
                        UserDefaults.standard.synchronize()
                        Helper.showAlert(fromController: self, withTitle: "Info", AndMessage: "Profile data updated successfully")
                        
                        
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                    //let user_data = (result as? [String : Any])?["user_data"] as? [String : Any]
                    // print(user_data!)
                    /*
                     let JSON = result as! NSDictionary
                     let message_code = (result as? [String : Any])?["message_code"]
                     let user_data = ((result as? [String : Any])?["user_data"] as? [String : Any])?["name"]
                     
                     let uData = JSON["user_data"] as! NSDictionary
                     print(uData["name"]!)
                     */
                    
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                // handle success
                
        }
    }
    //MARK :- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
        if(textField != txtFirstName || textField != txtLastName || textField != txtGender){
            animateViewMoving(up:true, moveValue: 150)
        }else{
            animateViewMoving(up:true, moveValue: 50)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called")
        if(textField != txtFirstName || textField != txtLastName || textField != txtGender){
            animateViewMoving(up:false, moveValue: 150)
        }else{
            animateViewMoving(up:false, moveValue: 50)
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    func myPhotos()
    {
        
        scrlEditProfile.isHidden = true
        scrlMyRace.isHidden = true
        collectionview.isHidden = false
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "apimyphotos","user_id":UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String] as [String : Any]
        //   UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = "No photo available now."
                    print(message_code)
                    if(message_code == "1"){
self.myPhotosArray = ((result as? [String : Any])?["galleries"] as? [Any])!
                        
                        
                        self.collectionview.reloadData()
                        print("Loading Data in tableview ............")
                        
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
                //  UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // handle success
                
        }
    }
    func myRace()
    {
        scrlEditProfile.isHidden = true
        scrlMyRace.isHidden = false
        collectionview.isHidden = true
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "apimyrace","user_id":UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String] as [String : Any]
        //   UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = (result as? [String : Any])?["message"] as! String
                    print(message_code)
                    if(message_code == "1"){
                        self.lblDistanceRun.text = ((result as? [String : Any])?["distance_run"] as? String)!  + "KM"
                        self.upcomingRaceArray = ((result as? [String : Any])?["upcoming_race"] as? [Any])!
                        self.completedRaceArray = ((result as? [String : Any])?["race_completed"] as? [Any])!
                        
                        Helper.setTableview(tblview:self.tblview)
                        print("Loading Data in tableview ............")
                        
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
                //  UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // handle success
                
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(completedRaceArray.count>0 && upcomingRaceArray.count>0){
            return 2
        }
        else if(completedRaceArray.count>0){
            return 1
        }
        else if(upcomingRaceArray.count>0){
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      //  let currSection = fetchedResultsController.sections?[section]
       // let title = currSection!.name
        
        // Dequeue with the reuse identifier
        let cell = self.tblview.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader")
        let header = cell as! TableSectionHeader
        if(upcomingRaceArray.count>0 && section == 0){
            header.lblRaceType.text = "UPCOMING"
        }else{
            header.lblRaceType.text = "COMPLETED"
        }
        
       
        
        return cell
    }
    // MARK : tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Total Count: \(completedRaceArray.count)")
        
        if(upcomingRaceArray.count>0 && section == 0){
            return upcomingRaceArray.count
        }else{
            return completedRaceArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        var cell: MyRacesCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyRacesCell
        if cell == nil {
            tableView.register(UINib(nibName: "MyRacesCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyRacesCell
        }
        let raceDict:[String:Any] = completedRaceArray[indexPath.row] as! [String : Any]
        cell.lblRaceTitle.text = (raceDict["race_title"] as! String)
        if(raceDict["race_date"] as! String != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "dd-MMM-yyyy";
            cell.lblRaceDate.text = dateFormatter2.string(from: dateFormatter.date(from: raceDict["race_date"]! as! String)!)
        }

        cell.lblRaceDistance.text = (raceDict["distance"] as! String) + "KM"
        cell.imgRace.sd_setImage(with: URL(string:(raceDict["race_image"] as! String)))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailsObj =  Helper.storyBoard().instantiateViewController(withIdentifier: "RacesDetails") as! RacesDetails
        detailsObj.raceDict=completedRaceArray[indexPath.row] as! [String:Any]
        self.navigationController?.pushViewController(detailsObj, animated: true)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myPhotosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // Set cell width to 100%
        
        return CGSize(width: (GlobalConstants.SCREEN_WIDTH/2)-1, height: GlobalConstants.SCREEN_WIDTH/2)
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! customCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
       // cell.lblTitle.text=myPhotosArray[indexPath.row];
         cell.imgview.sd_setImage(with: URL(string:(myPhotosArray[indexPath.row] as! String)))
        //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }

}

//
//  Inbox.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import Alamofire
class Inbox: BaseviewController , UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblview:UITableView!
    var orgnizorArray = [Any]()
    override func viewDidLoad(){
        super.viewDidLoad()
        self.tblview.isHidden=true
        self.loadOrg()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadOrg()
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "getorganiser" , "user_id" : UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String] as [String : Any]
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = "There is no coupon available for you."
                    print(message_code)
                    if(message_code == "1"){
                        self.orgnizorArray = ((result as? [String : Any])?["organisers"] as? [Any])!
                        
                        Helper.setTableview(tblview:self.tblview)
                        print("Loading Data in tableview ............")
                        
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Total Count: \(orgnizorArray.count)")
        return orgnizorArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        var cell: InboxCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? InboxCell
        if cell == nil {
            tableView.register(UINib(nibName: "InboxCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? InboxCell
        }
        
        
        let couponDict:[String:String] = orgnizorArray[indexPath.row] as! [String : String]
        cell.lblTitle.text = couponDict["company_name"]
        /*
        cell.lblCouponTitle.text = couponDict["coupon_name"]
        cell.lblExpireDate.text = couponDict["exp_date"]
        cell.lblMerchantName.text = couponDict["marchant_name"]
        cell.imgMerchantLogo.sd_setImage(with: URL(string:(couponDict["marchant_logo"]!)))
        */
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailsObj =  Helper.storyBoard().instantiateViewController(withIdentifier: "InboxDetails") as! InboxDetails
        let couponDict:[String:String] = orgnizorArray[indexPath.row] as! [String : String]
        
        detailsObj.orgId = couponDict["id"]!
        self.navigationController?.pushViewController(detailsObj, animated: true)
    }
}

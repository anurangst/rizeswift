//
//  Invoice.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 29/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit

class Invoice: BaseviewController {
    @IBOutlet weak var lblInvoiceDate:UILabel!
    @IBOutlet weak var lblInvoiceId:UILabel!
    @IBOutlet weak var lblCompanyName:UILabel!
    @IBOutlet weak var lblCompanyRegNo:UILabel!
    @IBOutlet weak var lblCompanyAddress:UILabel!
    @IBOutlet weak var lblRaceName:UILabel!
    @IBOutlet weak var lblRaceDate:UILabel!
    @IBOutlet weak var lblRaceTime:UILabel!
    @IBOutlet weak var lblRaceCategory:UILabel!
    @IBOutlet weak var lblRaceFee:UILabel!
    @IBOutlet weak var lblRacePaid:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnBackAction()
    {
        self.navigationController?.popViewController(animated: true)
    }

}

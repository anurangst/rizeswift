//
//  RacesDetails.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import Alamofire
class RacesDetails: BaseviewController , UITableViewDelegate ,UITableViewDataSource {
    @IBOutlet weak var tblview:UITableView!
    @IBOutlet weak var btnRegisterRace: UIButton!
     @IBOutlet weak var btnShowInvoice: UIButton!
    @IBOutlet weak var imgRace:UIImageView!
    @IBOutlet weak var lblRaceTitle:UILabel!
    @IBOutlet weak var lblRaceDate:UILabel!
    
    var categoryArray = [Any]()
    var selectedCategory:String!
    var raceDict: [String  : Any] = [:]
    var dictionary: [String:Int] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        print(raceDict)
        categoryArray = (raceDict["category"] as? Array)!
        lblRaceTitle.text=raceDict["race_title"] as? String
        Helper.setTableview(tblview:self.tblview)
          // print (raceDict["race_date"]  as! String)
        
        //http://gstdemo.com/races/webservice?action=apiraceseen&user_id=1&race_id=15
        imgRace.sd_setImage(with: URL(string:(raceDict["race_image"] as! String)))
        
        if(raceDict["is_registration_open"] as! String == "0"){
            btnRegisterRace.isHidden = true
        }
        
        if(raceDict["is_registered"] as! Int == 1){
            btnShowInvoice.isHidden = false
            btnRegisterRace.isHidden = true
        }else{
             btnShowInvoice.isHidden = true
        }
        
        
        if(raceDict["race_date"] as! String != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "dd-MMM-yyyy";
            lblRaceDate.text  = "Race date: " + dateFormatter2.string(from: dateFormatter.date(from: raceDict["race_date"]! as! String)!)
       }
 
 
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnInvoiceAction(sender:UIButton){
      self.navigationController?.pushViewController(Helper.storyBoard().instantiateViewController(withIdentifier: "invoice"), animated: true)
    }
    @IBAction func btnRegisterRaceAction(sender:UIButton)
    {
        if (selectedCategory != nil){
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "apiraceregister" , "category_id": selectedCategory , "race_id": raceDict["id"]! , "user_id":UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String] as [String : Any]
        print (parameters)
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    if(message_code == "1"){
                        self.btnRegisterRace.isHidden = true
                        Helper.showAlert(fromController: self, withTitle: "Success!", AndMessage: "You have registered this race successfully.")
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: "Sorry there are some internal error. Please try later.")
                    }
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
            }
        }else{
            Helper.showAlert(fromController: self, withTitle: "Info", AndMessage: "Select a category")
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Total Count: \(categoryArray.count)")
        return categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        var cell: receCategoryCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? receCategoryCell
        if cell == nil {
            tableView.register(UINib(nibName: "receCategoryCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? receCategoryCell
        }
        
        
        let raceDict:[String:Any] = categoryArray[indexPath.row] as! [String : Any]
        cell.lblTitle.text = raceDict["bundle_name"] as! String?
        cell.lblPrice.text = "$" + (raceDict["price"] as! String?)!
        cell.lblDistance.text = (raceDict["distance"] as! String?)! + "KM"
        
       
        
        //cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
      selectedCategory =  (categoryArray[indexPath.row] as! [String:Any])["catid"]  as! String
       
        
    }

}

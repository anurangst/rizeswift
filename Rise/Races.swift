//
//  Races.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import Alamofire

class Races: BaseviewController , UITableViewDelegate ,UITableViewDataSource {
    @IBOutlet weak var tblview:UITableView!
    @IBOutlet weak var btnUpcomingRace:UIButton!
    @IBOutlet weak var btnAllRace:UIButton!
    @IBOutlet weak var btnPopularRace:UIButton!
    @IBOutlet weak var viewSelectedBar:UIView!
    var raceArray =  [Any]()
    var upcomingRaceArray =  [Any]()
    var allRaceArray =  [Any]()
    var popularRaceArray =  [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblview.isHidden=true
        btnAllRace.isSelected = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
   
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.loadRace()
        self.viewSelectedBar.center.x = self.btnAllRace.center.x
    }
    func loadRace()
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "apiracelist", "user_id":UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String] as [String : Any]
        print(parameters)
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = (result as? [String : Any])?["message"] as! String
                    print(message_code)
                    if(message_code == "1"){
                        self.upcomingRaceArray = ((result as? [String : Any])?["upcoming_race"] as? [Any])!
                        self.allRaceArray = ((result as? [String : Any])?["all_race"] as? [Any])!
                        self.popularRaceArray = ((result as? [String : Any])?["popular_race"] as? [Any])!
                        self.raceArray = self.allRaceArray
                        Helper.setTableview(tblview:self.tblview)
                        print("Loading Data in tableview ............")
                        
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                    //let user_data = (result as? [String : Any])?["all_race"] as? [String : Any]
                    // print(user_data!)
                    /*
                     let JSON = result as! NSDictionary
                     let message_code = (result as? [String : Any])?["message_code"]
                     let user_data = ((result as? [String : Any])?["user_data"] as? [String : Any])?["name"]
                     
                     let uData = JSON["user_data"] as! NSDictionary
                     print(uData["name"]!)
                     */
                   // self.navigationController?.pushViewController(Helper.storyBoard().instantiateViewController(withIdentifier: "Races") , animated: true)
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
              //  UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // handle success
                
        }
    }

  @IBAction  func btnRaceType(sender:UIButton)
    {
        
        switch sender.tag {
        case 101:
            raceArray=upcomingRaceArray
            btnUpcomingRace.isSelected = true
            btnAllRace.isSelected = false
            btnPopularRace.isSelected = false
            UIView.animate(withDuration: 0.2, animations: {
                self.viewSelectedBar.center.x = self.btnUpcomingRace.center.x
            }, completion: {
                (value: Bool) in
                
            })
        case 102:
            raceArray=allRaceArray
            btnUpcomingRace.isSelected = false
            btnAllRace.isSelected = true
            btnPopularRace.isSelected = false
            UIView.animate(withDuration: 0.2, animations: {
                self.viewSelectedBar.center.x = self.btnAllRace.center.x
            }, completion: {
                (value: Bool) in
                
            })
        case 103:
            raceArray=popularRaceArray
            btnUpcomingRace.isSelected = false
            btnAllRace.isSelected = false
            btnPopularRace.isSelected = true
            UIView.animate(withDuration: 0.2, animations: {
                self.viewSelectedBar.center.x = self.btnPopularRace.center.x
            }, completion: {
                (value: Bool) in
                
            })
        default:
            raceArray =  [Any]()
        }
        tblview.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Total Count: \(raceArray.count)")
        return raceArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        var cell: RaceCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RaceCell
        if cell == nil {
            tableView.register(UINib(nibName: "RaceCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RaceCell
        }
        let raceDict:[String:Any] = raceArray[indexPath.row] as! [String : Any]
        cell.lblRaceTitle.text = (raceDict["race_title"] as! String)
        cell.imgRace.sd_setImage(with: URL(string:(raceDict["race_image"] as! String)))
       
        
        
        cell.selectionStyle = .none
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailsObj =  Helper.storyBoard().instantiateViewController(withIdentifier: "RacesDetails") as! RacesDetails
        detailsObj.raceDict=raceArray[indexPath.row] as! [String:Any]
        self.navigationController?.pushViewController(detailsObj, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

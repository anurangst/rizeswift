//
//  Camera.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
class Camera: BaseviewController , AVCapturePhotoCaptureDelegate ,UIScrollViewDelegate {
    @IBOutlet weak var scrlview:UIScrollView!
    @IBOutlet weak var capturedImage: UIImageView!
    @IBOutlet weak var previewView: UIView!
    var currentPage = 0
    var tempImageview :UIImageView!
    var selectedRaceDict: [String:String] = [:]
    var raceArray = [Any]()
    var frameImageArray = [UIImageView]()
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCapturePhotoOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var sessionOutputSetting = AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecJPEG]);
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrlview.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(Camera.receivedFrameNotification(notification:)), name: Notification.Name("FrameSelectedNotification"), object: nil)
        todaysRace();
    }
   
    func receivedFrameNotification(notification: Notification){
        scrlview.contentOffset=CGPoint.init(x: (notification.userInfo?["index"] as! Int) * GlobalConstants.SCREEN_WIDTH, y: 0)
        selectedRaceDict = (raceArray[notification.userInfo?["index"] as! Int]) as! [String:String]
        print(raceArray[notification.userInfo?["index"] as! Int])
    }
    
    override func viewWillAppear(_ animated: Bool) {
           captureSession = AVCaptureSession()
           captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
           let backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
           // var sessionOutputSetting = AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecJPEG]);
        
            var error : NSError?
            let input = try! AVCaptureDeviceInput (device: backCamera)
            if (error == nil && captureSession?.canAddInput(input) != nil) {
                captureSession?.addInput(input)
                stillImageOutput = AVCapturePhotoOutput()
                if (captureSession?.canAddOutput(stillImageOutput) != nil) {
                    captureSession?.addOutput(stillImageOutput)
                    previewLayer = AVCaptureVideoPreviewLayer (session: captureSession)
                    previewLayer?.videoGravity = AVLayerVideoGravityResizeAspect
                    previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait
                    previewLayer?.frame = self.previewView.bounds
                    previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
                    previewView.layer.addSublayer(previewLayer!)
                    captureSession?.startRunning()
                }
            }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer?.frame = previewView.bounds
    }
 
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        currentPage =  Int(scrlview.contentOffset.x / scrlview.frame.size.width)
        print("Current Page: \(currentPage)")
    }
    func todaysRace()
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        let parameters = ["action": "apitodaysracelist"] as [String : Any]
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = (result as? [String : Any])?["message"] as! String
                    print(message_code)
                    if(message_code == "1"){
                        self.raceArray = ((result as? [String : Any])?["todays_race"] as? [Any])!
                        self.loadFrameInScrollview()
                        print("Loading Data in tableview ............")
                        
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                
                //  UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // handle success
                
        }
    }

    func loadFrameInScrollview()
    {
        
        var x: Int = 0
        for i in 0..<raceArray.count {
            let img = UIImageView.init(frame: CGRect.init(x: x, y: 0, width: GlobalConstants.SCREEN_WIDTH, height: Int(scrlview.frame.size.height)))
            
            //race_frame
            img.sd_setImage(with: URL(string:((raceArray[i] as! [String: Any])["race_frame"] as! String)))
            scrlview.addSubview(img)
            x += GlobalConstants.SCREEN_WIDTH
            
            frameImageArray.append(img)
            print ("X is : \(x)")
        }
        tempImageview = frameImageArray[0]
        selectedRaceDict = raceArray[0] as! [String:String]
        let width = GlobalConstants.SCREEN_WIDTH * raceArray.count
        scrlview.contentSize=CGSize.init(width: CGFloat(width) , height: scrlview.frame.size.height)
    }
    @IBAction func btnCaptureAction(sender:UIButton)
    {
        
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160,
                             ]
        settings.previewPhotoFormat = previewFormat
        self.stillImageOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        print("Captured.........")
        if let error = error {
            print(error.localizedDescription)
        }
        
        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            self.capturedImage.image = UIImage(data: dataImage)
            
         
            if let capturedUIImage =  Helper.resizeImage(image: UIImage(data: dataImage)!, targetSize: CGSize.init(width: 375, height: 375)) as UIImage?
            {
                
                capturedImage.image = capturedUIImage
                self.previewView.isHidden = true
                self.takeConfirmaionToSave()
               
            }
            //if let data = UIImagePNGRepresentation(capturedImage) as NSData? {
               // self.uploadImage(capturedImage: capturedImage)
            //}
        } else {
            
        }
    }
    
    func takeConfirmaionToSave(){
        
        let alertController = UIAlertController(title:title, message:"Are you sure to save it?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
             self.uploadImage(capturedImg: self.capturedImage.image!)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.previewView.isHidden = false
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func uploadImage(capturedImg:UIImage)
    {
        var newImage:UIImage
        if(tempImageview.image != nil){
        let topImage = tempImageview.image
        let bottomImage = capturedImg
        let size = CGSize(width:topImage!.size.width, height:topImage!.size.height)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        bottomImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        topImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        }else{
            newImage = capturedImg
        }
        let user_id = UserDefaults.standard.dictionary(forKey: "user_data")?["id"] as! String
        let race_id = selectedRaceDict["id"]
        let generatedUrl = GlobalConstants.BASE_URL + "?action=imgupload&race_id=" + race_id! + "&user_id=" + user_id
        
        print (generatedUrl)
        let URL = try! URLRequest(url: generatedUrl, method: .post, headers: nil)
         SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(UIImagePNGRepresentation(newImage
                )!, withName: "fileToUpload", fileName: "picture.png", mimeType: "image/png")
        }, with: URL, encodingCompletion: {
            encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print("SUCCESS RESPONSE: \(response.result.value!)")
                    SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                    self.previewView.isHidden = false
                }
            case .failure(let encodingError):
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                print("ERROR RESPONSE: \(encodingError)")
            }
        })
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnBackAction(sender:UIButton)
    {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFrameAction(sender:UIButton){
        
        let todaysRaceVC = Helper.storyBoard().instantiateViewController(withIdentifier: "todaysRace") as! todaysRace
        todaysRaceVC.raceArray = raceArray
        self.navigationController?.pushViewController(todaysRaceVC , animated: true)
    }

}

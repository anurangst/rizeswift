
//
//  Helper.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import Foundation
import UIKit
class Helper{
     var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    static func postRequest() -> [String:String] {
        // do a post request and return post data
        return ["someData" : "someData"]
    }
    
    static func loadTabBar()
    {
        self.appDelegate().window??.rootViewController = tabController()
    }
    
    
    static func appDelegate()-> UIApplicationDelegate
    {
        return UIApplication.shared.delegate!
    }
    static func storyBoard()-> UIStoryboard
    {
      return UIStoryboard.init(name: "Main", bundle: nil);
    }
    
    static func setTableview(tblview:UITableView)
    {
        var frame = CGRect.zero
        frame.size.height = 1
        tblview.isHidden=false
        tblview.separatorStyle = .none
        tblview.tableFooterView = UIView(frame: frame)
        tblview.tableHeaderView = UIView(frame: frame)
        tblview.reloadData()
    }
    static func isStringEmpty(stringValue:String) -> Bool
    {
        var returnValue = false
        
        if stringValue.isEmpty  == true
        {
            returnValue = true
            return returnValue
        }
        
        // Make sure user did not submit number of empty spaces
        let str   = stringValue.trimmingCharacters(in: NSCharacterSet.whitespaces)
        
        if(str.isEmpty == true)
        {
            returnValue = true
            return returnValue
            
        }
        return returnValue
    }
    
  static  func showAlert(fromController controller: UIViewController, withTitle title: String, AndMessage message:String) {
        // Create the alert controller
        let alertController = UIAlertController(title:title, message:message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
    /*
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        */
        // Add the actions
        alertController.addAction(okAction)
       // alertController.addAction(cancelAction)
        
        // Present the controller
        controller.present(alertController, animated: true, completion: nil)
    }
    
   static func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    static func setPaddingForTextField(textfield: UITextField)  {
        let leftPaddingView = UIView(frame:CGRect(x: 0, y: 0, width:10, height:textfield.frame.size.height))
        textfield.leftView = leftPaddingView
        textfield.leftViewMode = UITextFieldViewMode.always
    }
    
   static func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
    
    
    
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            
          newSize =  CGSize.init(width: size.width * heightRatio, height: size.height * heightRatio)
            
        } else {
           newSize =  CGSize.init(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
          let rect = CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    
    static func combineImages(img1:UIImage){
        let topImage = UIImage(named: "image1.png")
        let bottomImage = UIImage(named: "image2.png")
         let size = CGSize(width: topImage!.size.width, height: topImage!.size.height + bottomImage!.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        topImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: topImage!.size.height))
        bottomImage?.draw(in: CGRect.init(x: 0, y: topImage!.size.height, width: size.width, height: bottomImage!.size.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        
    }
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

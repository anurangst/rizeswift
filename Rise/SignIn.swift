//
//  SignIn.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 12/12/16.
//  Copyright © 2016 GOLDSYNC TECH. All rights reserved.
//

import UIKit
import Alamofire
class SignIn: BaseviewController {
    @IBOutlet weak var textEmail:UITextField!
    @IBOutlet weak var textPassword:UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        Helper.setPaddingForTextField(textfield: textEmail)
        Helper.setPaddingForTextField(textfield: textPassword)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(false)
        Helper.setPaddingForTextField(textfield: textEmail)
        Helper.setPaddingForTextField(textfield: textPassword)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
     @IBAction func btnSignInAction(sender:UIButton)
    {
        if (Helper.isStringEmpty(stringValue: textEmail.text!))
        {
            Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: "Please Enter Email Id.")
        }
        else if(!Helper.validateEmail(enteredEmail: textEmail.text!))
        {
            Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: "Please Enter Valid Email Id.")
        }
        else if (Helper.isStringEmpty(stringValue: textPassword.text!))
        {
            Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: "Please Enter Password.")
        }
        else
        {
            self.login()
        }

    }
    
    func login()
    {
        SingletonObject.sharedInstance.showActivityIndicator(uiView: self.view)
        print("Login function called.........")
        let parameters = ["action": "apilogin", "email":textEmail.text!, "password": textPassword.text!] as [String : Any]
        
        Alamofire.request(GlobalConstants.BASE_URL, method: .post, parameters: parameters)
            .responseJSON{ response in
                SingletonObject.sharedInstance.hideActivityIndicator(uiView: self.view)
                //debugPrint(response)
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                //  https://grokswift.com/rest-with-alamofire-swiftyjson/
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    
                    
                }
                //to get status code
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    print("Data............")
                    let message_code = (result as? [String : Any])?["message_code"] as! String
                    let message = (result as? [String : Any])?["message"] as! String
                    //let userDict = (result as? [String : Any])?["user_data"] as! [String : Any]
                    print(message_code)
                    if(message_code == "1"){
                        UserDefaults.standard.set((result as? [String : Any])?["user_data"], forKey: "user_data")
                        UserDefaults.standard.synchronize()
                       Helper.loadTabBar()
                    }else{
                        Helper.showAlert(fromController: self, withTitle: "Error!", AndMessage: message)
                    }
                    //let user_data = (result as? [String : Any])?["user_data"] as? [String : Any]
                   // print(user_data!)
                    /*
                     let JSON = result as! NSDictionary
                     let message_code = (result as? [String : Any])?["message_code"]
                     let user_data = ((result as? [String : Any])?["user_data"] as? [String : Any])?["name"]
                     
                     let uData = JSON["user_data"] as! NSDictionary
                     print(uData["name"]!)
                     */
                     
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get todo object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                print(json)
                
                guard response.result.isSuccess else {
                    // handle failure
                    return
                }
                // handle success
                
        }
    }
    //MARK :- TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
        animateViewMoving(up:true, moveValue: 50)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called")
        animateViewMoving(up:false, moveValue: 50)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }

}

//
//  MyRacesCell.swift
//  Rise
//
//  Created by GOLDSYNC TECH on 11/01/17.
//  Copyright © 2017 GOLDSYNC TECH. All rights reserved.
//

import UIKit

class MyRacesCell: UITableViewCell {
    @IBOutlet weak var lblRaceTitle:UILabel!
    @IBOutlet weak var lblRaceDate:UILabel!
    @IBOutlet weak var lblRaceDistance:UILabel!
    @IBOutlet weak var imgRace:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
